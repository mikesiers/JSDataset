# JSDataset

JSDataset is a library for parsing, manipulating, converting and storing
common formats of flat structured dataset files. The advantage of JSDataset is that it
is implemented in pure *Javascript*. This allows it to be easily integrated into
a web-based data science application. Note that this is not suitable for big data projects,
but could prove useful for smaller data science projects.

## How to Use

JSDataset is easy to get up and running in your web application. Here are some steps to parse in
an arff file, access some elements, and change some values.

### Step One: Including JSDataset.js

Let's start with the following minimal web page which allows the user to upload a file.
This file is available in this repo at *demo/demo.html*.
Note that in the head of the html, the JSDataset.js file is included. Gee, that was easy..
In step two, an example of how to call JSDataset functions is described.

```HTML
<html>
	<head>
		<title>JSDataset Demo</title>
		<script type="text/javascript" src="JSDataset.js"></script>
	</head>
	<body>
		<!--The following solution is taken from http://tinyurl.com/ju6fguj -->
		<form id = "uploadForm">
			<p>
				<input name="fileUpload" name="files[]" multiple="" type="file" />
				<textarea id="text" rows="20" cols="40">nothing loaded</textarea>
			</p>
		</form>
		<script type="text/javascript" src="demo.js"></script>
	</body>
</html>
```

### Step Two: Parsing in a dataset.

The following code shows *demo.js* which is the main javascript file for the demo.
This code parses the file which was uploaded using the input from *demo.html*.
On the line *filecontent = evt.target.result;*, the file that the user selected is
stored as a string in the *filecontent* variable. This string is what is needed to
instantiate a JSDataset object. The next line of code instantiates a JSDataset object
by parsing the filecontent string as an ARFF file. A similar thing could be done for
a csv file.

```Javascript
// The following solution was taken from http://tinyurl.com/ju6fguj
document.forms['uploadForm'].elements['fileUpload'].onchange = function(evt) {
	if (!window.FileReader) return;	// Browser is not compatible

	var reader = new FileReader();

	reader.onload = function(evt) {
		if (evt.target.readyState != 2) return;
		if (evt.target.error) {
			alert('Error while reading file');
			return
		}

		filecontent = evt.target.result;

		var dataset = parse_ARFF(filecontent);
		console.log(dataset);

		document.forms['uploadForm'].elements['text'].value = evt.target.result;
	};

	reader.readAsText(evt.target.files[0]);
};
```

### Step Three: Inspecting the Loaded Data

MORE COMING

## Architecture

The following figure is an image of the current class structure.

![JSDataset Class Diagram](https://github.com/mikesiers/JSDataset/blob/master/readmeImages/classDiagram.png)

## Currently Supported Formats:

- [x] arff
- [x] csv 
- [ ] data + names (C4.5 format)
- [ ] libsvm
- [ ] json

## License

GNU General Public License
