var assert = require('assert');
var fs = require('fs');
var JSDataset = require('../JSDataset.js');

var adult_file_string = fs.readFileSync('TestData/adult.arff', 'utf8');
var adult_dataset = JSDataset.parse_ARFF(adult_file_string);

var pima_file_string = fs.readFileSync('TestData/pima.csv', 'utf8');
var pima_dataset = JSDataset.parse_CSV('pima', pima_file_string, ',', '"');

describe('parse_ARFF', function() {
	it('parses an ARFF file and stores it in memory', function() {
		assert.equal(adult_dataset.get_name(), 'adult');
		assert.equal(adult_dataset.get_attribute(4).get_name(), 'education-num');
		assert.equal(adult_dataset.get_num_attributes(), 15);
		assert.equal(adult_dataset.get_num_records(), 32561);
		assert.equal(adult_dataset.get_record(0).get_value(3).get_value(),
			'Bachelors');
	});
});

describe('parse_CSV', function() {
	it('parses a csv file and stores it in memory', function() {
		console.log(pima_dataset.get_name());
		assert.equal(pima_dataset.get_name(), 'pima');
		assert.equal(pima_dataset.get_attribute(4).get_name(), 'Insu');
		assert.equal(pima_dataset.get_num_attributes(), 9);
		assert.equal(pima_dataset.get_num_records(), 768);
		assert.equal(pima_dataset.get_record(0).get_value(3).get_value(), 35);
	});
});

describe('predict_attribute_type', function() {
	it('correctly predicts a categorical attribute', function() {
		var values = ['red', 'blue', 'green', 'green', 'white', 'brown', 'black'];
		var prediction = JSDataset.predict_attribute_type(values);
		assert.equal(prediction, 'Categorical');
	});
	it('correctly predicts a numeric attribute', function() {
		var values = ['12', '13.4', '1', '-20', '-6.61', '0'];
		var prediction = JSDataset.predict_attribute_type(values);
		assert.equal(prediction, 'Numeric');
	});
});

describe('values_from_csv_line', function() {
	it('returns the values from a single line of a csv file', function() {
  	var csv_line = '"Hello, World!",a,fred,3.407,42,cake = lie!';
		var values = ['Hello, World!', 'a', 'fred', '3.407', '42', 'cake = lie!'];
		var function_output = JSDataset.values_from_csv_line(csv_line, ',', '"');
		assert.deepEqual(values, function_output);
	});
});

describe('Categorical_Value', function() {
	describe('get_value', function() {
		it('gets the value of the object', function() {
			var cat_val = new JSDataset.Categorical_Value('TestValue');
			assert.equal('TestValue', cat_val.get_value());
		});
	});
});

describe('Numeric_Value', function() {
	describe('get_value', function() {
		it('gets the value of the object', function() {
			var num_val = new JSDataset.Numeric_Value(-33.860);
			assert.equal(-33.860, num_val.get_value());
		});
	});
});

describe('Record', function() {
	describe('get_value', function() {
		it('gets the value object at the requested index', function() {
			var val_one = new JSDataset.Categorical_Value('Test Val');
			var val_two = new JSDataset.Numeric_Value(32);
			var val_three = new JSDataset.Categorical_Value('2nd Test Val');
			var values = [val_one, val_two, val_three];
			var record = new JSDataset.Record(values);
			assert.equal(record.get_value(0).get_value(), 'Test Val');
			assert.equal(record.get_value(1).get_value(), 32);
			assert.equal(record.get_value(2).get_value(), '2nd Test Val');
		});
	});
});

describe('Attribute', function() {
	describe('get_name', function() {
		it('returns the name of the attribute', function() {
			var name_one = 'DayOfWeek';
			var name_two = 'Day of the week';
			var attribute = new JSDataset.Attribute(name_one,
				new JSDataset.Dataset());
			assert.equal(attribute.get_name(), name_one);
			attribute = new JSDataset.Attribute(name_two);
			assert.equal(attribute.get_name(), name_two);
		});
	});
});

describe('Categorical_Attribute', function() {
	var name = 'Test Attribute';
  var possible_values = ['value one', 'value two', 'value three'];
  var attribute = new JSDataset.Categorical_Attribute(name, possible_values,
		new JSDataset.Dataset());

	describe('is_possible_value', function() {
		it('checks if a requested value is a possible value', function() {
			assert(attribute.is_possible_value('value three'));
			assert(!(attribute.is_possible_value('value four')));
		});
	});
});

describe('Numeric_Attribute', function() {
	describe('max', function() {
		it('Returns the maximum existing value for this attribute', function() {
			assert.equal(adult_dataset.get_attribute(0).max(), 90);
		});
	});
	describe('min', function() {
		it('Returns the minimum existing value for this attribute', function() {
			assert.equal(adult_dataset.get_attribute(0).min(), 17);
		});
	});
});

describe('Dataset', function() {
	var dummy_dataset = new JSDataset.Dataset();
	describe('get_name', function() {
  	it('gets the name of the Dataset', function() {
			assert.equal(adult_dataset.get_name(), 'adult');
  	});
  });
	describe('get_record', function() {
  	it('returns the record at the requested index', function() {
			assert.equal(adult_dataset.get_record(3).get_value(2).get_value(),
				234721);
  	});
  });
	describe('get_num_records', function() {
  	it('gets the number of records in the Dataset', function() {
			assert.equal(adult_dataset.get_num_records(), 32561);
  	});
  });
	describe('get_attribute', function() {
  	it('gets the attribute at the requested index', function() {
			assert.equal(adult_dataset.get_attribute(1).get_name(), 'workclass');
  	});
  });
	describe('get_num_attributes', function() {
  	it('gets the number of records in the Dataset', function() {
			assert.equal(adult_dataset.get_num_attributes(), 15);
  	});
  });
	describe('set_name', function() {
  	it('sets the name of the dataset with the passed name', function() {
			dummy_dataset.set_name('dummy');
			assert.equal(dummy_dataset.get_name(), 'dummy');
  	});
  });
	describe('add_attribute', function() {
		it('adds the passed attribute to the dataset', function() {
			dummy_dataset.add_attribute(
				new JSDataset.Numeric_Attribute('height', dummy_dataset));
			assert.equal(dummy_dataset.get_attribute(0).get_name(), 'height');
		});
	});
	describe('add_record', function() {
		it('adds the passed record to the dataset', function() {
			var dummy_record = new JSDataset.Record([
				new JSDataset.Numeric_Value(182)]);
			dummy_dataset.add_record(dummy_record);
			assert.equal(dummy_dataset.get_record(0).get_value(0).get_value(), 182);
		});
	});
	describe('index_of_attribute', function() {
  	it('gets the index of the passed attribute, -1 if not found', function() {
			var attribute = adult_dataset.get_attribute(4);
			var index = adult_dataset.index_of_attribute(attribute);
			assert.equal(index, 4);
			attribute = new JSDataset.Attribute();
			index = adult_dataset.index_of_attribute(attribute);
			assert.equal(index, -1);
  	});
  });
	describe('all_attribute_values', function() {
  	it('returns an array of all values for that attribute', function() {
			var attribute = adult_dataset.get_attribute(0);
			var values = (adult_dataset.all_attribute_values(attribute));
			assert.equal(values[2], 38);
  	});
  });
});
