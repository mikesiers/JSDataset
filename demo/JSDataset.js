/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * This file contains a class for representation of a dataset which can be used
 * for data science. It also provides functionality for reading a storing a
 * dataset from file.
 * 
 * Original Programmer: Michael J. Siers */

"use strict";

/**
 * Parses an .ARFF file.
 * @param {String} file_string -  The .ARFF file represented as a String
 * @returns {Dataset} The result as a Dataset object.
 */
function parse_ARFF(file_string) {
	var dataset = new Dataset();
  var lines = file_string.split("\n");

  var line_index = 0;
  // Iterate over lines until the relation tag is found.
  while (!(lines[line_index].toLowerCase().startsWith("@relation "))) {
    line_index++;
  }
  // The current line index should be the dataset name.
  var name = lines[line_index].toLowerCase().split("@relation ")[1];
	name = name.trim(); // Remove any new line characters
	dataset.set_name(name);

  // Iterate over lines until an attribute tag is found.
  while (!(lines[line_index].toLowerCase().startsWith("@attribute "))) {
    line_index++;
  }

  // Parse the following attributes.
  while (lines[line_index].toLowerCase().startsWith("@attribute ")) {
    var current_line = lines[line_index];
    var attribute_string = current_line.toLowerCase().split("@attribute ")[1];

    /* Split the string by " ". The attribute name should be in the first
     * position. */
    var splits = attribute_string.split(" ");
		var attribute_name = splits[0];

    // Check whether the current attribute is numeric or categorical
    if (splits[1].startsWith("numeric")) {  // The attribute is numeric
			dataset.add_attribute(new Numeric_Attribute(attribute_name, dataset));
    }
    else if (splits[1].startsWith("{")) {  // The attribute is categorical 
      /* Take the substring from the original line string that starts with "{"
       * Necessary for when there is  whitespace inside the braces. */
      var values_string = attribute_string.split("{")[1];

      /* Cut out all the whitespaces here since they are meaningless and have
       * potential to make parsing the attribute names harder. */
      values_string = values_string.replace(/\s/g, '');

      // Now cut off the last character ("}"), then separate the string by ",".
      values_string = values_string.slice(0, -1);
			var possible_values = values_string.split(",");

			dataset.add_attribute(new Categorical_Attribute(attribute_name,
				possible_values, dataset));
    }

    line_index++;
  }

  // Iterate over lines until the data tag is found.
  while (!(lines[line_index].toLowerCase().startsWith("@data"))) {
    line_index++;
  }
	line_index++;

  // Parse the following records.
  while (lines[line_index]) {
    var record_string = lines[line_index];

    // Remove whitespace for parsing simplicity.
    record_string = record_string.replace(/\s/g, '');
    var value_strings = record_string.split(",");

		// Instantiate a Categorical_Attribute or Numeric_Attribute from the
		// value_strings.
		var record_values = [];
		for (var i = 0; i < value_strings.length; i++) {
			if (dataset.get_attribute(i) instanceof Categorical_Attribute) {
				record_values.push(new Categorical_Value(value_strings[i]));
			}
			else if (dataset.get_attribute(i) instanceof Numeric_Attribute) {
				record_values.push(new Numeric_Value(Number(value_strings[i])));
			}
		}

		dataset.add_record(new Record(record_values));
    line_index++;
  }

  return dataset;
}
//exports.parse_ARFF = parse_ARFF;

/**
 * Parses a .csv file.
 * @param {String} name - A name for the dataset. e.g., anneal.
 * @param {String} file_string - The .csv file represented as a String
 * @param {Char} delimiter - what the file uses to separate values (usually ,)
 * @param {Char} quotation_char - what the file uses to enclose quotation.
 * @returns {Dataset} The result as a Dataset object.
 */
function parse_CSV(name, file_string, delimiter, quotation_char) {
	var dataset = new Dataset();
	dataset.set_name(name);

  var lines = file_string.split("\n");

  // Read in the header line.
  var header = lines[0];
  var attribute_names = values_from_csv_line(header, delimiter,
		quotation_char);

  // Start reading in the data, starting at the second line.
	var all_record_values = [];	// Used to all_attribute_values later.
  var line_index = 1;
  while (lines[line_index]) {
    var line = lines[line_index];
    var record_values = values_from_csv_line(line, delimiter, quotation_char);
		all_record_values.push(record_values);
    line_index++;
  }

	// Get an array of every value for each Attribute.
  // e.g., The first array would have all the existing values for the first
  // attribute.
  // This was solved by using the solution found at http://tinyurl.com/jmqm5x9.
  var all_attribute_values = all_record_values[0].map(function(col, i) {
		return all_record_values.map(function(row) {
			return row[i];
		})
	});

  // Guess the attribute types.
	var attribute_types = [];
	for (var i = 0; i < all_attribute_values.length; i++) {
		attribute_types.push(predict_attribute_type(all_attribute_values[i]));
	}

  // Calculate the possible values for categorical attributes.
	var possible_values = [];
	for (var i = 0; i < attribute_types.length; i++) {
		if (attribute_types[i] == 'categorical') {
			// The following solution is a very clever and concise solution. I wanted
			// to get the distinct values from an array using pure javascript.
			// The solution was taken from http://tinyurl.com/got7pyy
			possible_values.push(all_attribute_values[i].filter(function(x, j, a) {
				return a.indexOf(x) == j;
			}));
		}
	}

	// Add all the attributes to the Dataset.
	for (var i = 0; i < attribute_types.length; i++) {
		if (attribute_types[i] == 'categorical') {
			dataset.add_attribute(new Categorical_Attribute(attribute_names[i],
				possible_values[i], dataset));
		}
		else if (attribute_types[i] = 'numeric') {
			dataset.add_attribute(new Numeric_Attribute(attribute_names[i],
				dataset));
		}
	}

	// Create all the record objects and add them to the dataset object.
	var records = [];
	for (var i = 0; i < all_record_values.length; i++) {
		var current_record_values = [];
		for (var j = 0; j < attribute_types.length; j++) {
			if (attribute_types[j] == 'categorical') {
				var new_value = new Categorical_Value(all_record_values[i][j]);
			}
			else if (attribute_types[j] == 'numeric') {
				var new_value = new Numeric_Value(all_record_values[i][j]);
			}
			current_record_values.push(new_value);
		}
		dataset.add_record(new Record(current_record_values));
	}

	return dataset;
}
//exports.parse_CSV = parse_CSV;

// ---- ---- ---- HELPER FUNCTIONS ---- ---- ----

/**
 * Given a set of attribute values, this function returns an intelligent guess
 * of the attribute's type. Each attribute will either be 'numeric' or
 * 'nominal'. This is done by predicting each values' type (numeric or nominal)
 * then finally predicting the attribute's type as the most commonly predicted
 * value type.
 *
 * @param {String|Array} attribute_values An array of all the values that
 * appear at least once within the Records array of a Dataset object.
 *
 * @returns {String} "Numeric" if the attribute is predicted as numeric,
 * "Categorical" if the attribute is predicted as categorical.
 */
function predict_attribute_type(attribute_values) {
  // Counts for categorical and numeric predictions
  var numeric_count = 0;
  var categorical_count = 0;

  /* Loop over all attribute values, if the current value is predicted as a
   * number, then increment the numeric count, otherwise increment the
   * categorical count. */
  var values_index = 0;
  while (attribute_values[values_index]) {
    if (!isNaN(attribute_values[values_index])) {
      numeric_count++;
    }
    else {
      categorical_count++;
    }
		values_index++;
  }

  /* Return the most commonly predicted value type. NOTE: This function will
	 * return "Categorical" if the votes are equal. */
  if (numeric_count > categorical_count) {
    return "Numeric";
  }
  else {
    return "Categorical";
  }
}
//exports.predict_attribute_type = predict_attribute_type;

/**
 * Given a line from a csv file, this function returns an array of all the
 * values in that line.
 * 
 * @param {String} csv_line The line from a csv file to get the values from.
 * @param {char} delimiter The delimiter used in the csv file.
 * @param {char} quotationChar The character used to denote quotation.
 *
 * @returns {String|Array} An array of values represented as strings.
 */
function values_from_csv_line(csv_line, delimiter, quotation_char) {
  var current_value = "";
  var record_values = [];
  var reading_quote = false;
  var char_index = 0;
    
  /* Loop over each char. If it's not a delimiter or a quotation character,
	 * then add it to a current_value variable. If it's the quotation character,
	 * toggle the reading_quote variable. If it's the delimiter and a quote is
	 * currently not being read, then add the current_value to the record_values
	 * array. Otherwise, add the character to the current_value. */
  while (char_index < csv_line.length) {
    var c = csv_line[char_index];
    if (c == quotation_char) {
      reading_quote = !reading_quote;
    }
    else if (c == delimiter && !reading_quote) {
      record_values.push(current_value);
      current_value = "";
    }
		else if (char_index == csv_line.length - 1) {
			current_value += c;
			record_values.push(current_value);
		}
    else {
      current_value += c;
    }
		char_index++;
  }
  return record_values;
}
//exports.values_from_csv_line = values_from_csv_line;

// ---- ---- ---- CLASS DEFINITIONS ---- ---- ----

/**
 * @class
 * @classdesc For representing a Value within a record.
 */
var Value = function() {
}

/**
 * A virtual method which returns the value of the value object.
 */
Value.prototype.get_value = function() {
}

/**
 * @class
 * @classdesc A subclass of 'Value' which represents a categorical value.
 * @param {String} value - The categorical value as a string.
 */
function Categorical_Value(value) {
	Value.call(this);
	/** @member {String} */
  this.value = value;
}
Categorical_Value.prototype = Object.create(Value.prototype);
Categorical_Value.prototype.constructor = Categorical_Value;
//exports.Categorical_Value = Categorical_Value;

/**
 * Overrides Value.get_value(). 
 * @returns {String} The categorical value of this Value object.
 */
Categorical_Value.prototype.get_value = function() {
  return this.value;
}

/**
 * @class
 * @classdesc A subclass of 'Value' which represents a numeric value.
 * @param {Number} value - The numeric value.
 */
function Numeric_Value(value) {
	Value.call(this);
	/** @member {Double} */
	this.value = value;
}
Numeric_Value.prototype = Object.create(Value.prototype);
Numeric_Value.prototype.constructor = Numeric_Value;
//exports.Numeric_Value = Numeric_Value;

/**
 * Overrides Value.get_value().
 * @returns {Double} The numeric value of this Value object.
 */
Numeric_Value.prototype.get_value = function() {
  return this.value;
}

/**
 * @class
 * @classdesc For representing a record within a dataset.
 * @param {Value[]} values - The values for the attributes of this record.
 */
var Record = function(values) {
  /** @member {Value[]} */
	this.values = values; // e.g., (1, 'red', TRUE)
}
//exports.Record = Record;

/**
 * Returns the the record's value which has the index passed to the function.
 * @param {Number} index - The index of the attribute to get the value of.
 * @returns {Value} This Record object's value at the requested index.
 */
Record.prototype.get_value = function(index) {
  return this.values[index];
}

/**
 * @class
 * @classdesc For representing an Attribute within a dataset.
 * @param {String} name - This attribute's name e.g., colour, day, height.
 * @param {Dataset} dataset_reference - The dataset it will belong to.
 */
var Attribute = function(name, dataset_reference) {
  /** @member {String} */
	this.name = name;  // e.g., (Color, Age)
	/** @member {Dataset} */
	this.dataset_reference = dataset_reference;
}
//exports.Attribute = Attribute;

/**
 * Returns the name of the attribute.
 * @returns {String} The name of this attribute.
 */
Attribute.prototype.get_name = function() {
  return this.name;
}

/**
 * @class
 * @classdesc A subclass of 'Attribute' representing a categorical attribute.
 * @param {String} name - This attribute's name e.g., colour, day, gender.
 * @param {String[]} possible_values - the values that this attribute can have.
 * @param {Dataset} dataset_reference - The dataset it will belong to.
 */
function Categorical_Attribute(name, possible_values, dataset_reference) {
  Attribute.call(this, name, dataset_reference);
	/** @member {String[]} */
	this.possible_values = possible_values;
}
Categorical_Attribute.prototype = Object.create(Attribute.prototype);
Categorical_Attribute.prototype.constructor = Categorical_Attribute;
//exports.Categorical_Attribute = Categorical_Attribute;

/**
 * Checks if the value is a possible value for this attribute.
 * @param {String} value - The value to check.
 * @returns True if possible, False if not.
 */
Categorical_Attribute.prototype.is_possible_value = function(value) {
	return (this.possible_values.indexOf(value) > -1);
}

/**
 * @class
 * @classdesc A subclass of 'Attribute' which represents a numeric attribute.
 * @param {String} name - This attribute's name e.g., height, miles, age
 * @param {Dataset} dataset_reference - The dataset it will belong to.
 */
function Numeric_Attribute(name, dataset_reference) {
  Attribute.call(this, name, dataset_reference);
}
Numeric_Attribute.prototype = Object.create(Attribute.prototype);
Numeric_Attribute.prototype.constructor = Numeric_Attribute;
//exports.Numeric_Attribute = Numeric_Attribute;

/**
 * Calculates the minimum value of this attribute within the dataset.
 * @returns the minimum value of this attribute within the dataset.
 */
Numeric_Attribute.prototype.min = function() {
	var values = this.dataset_reference.all_attribute_values(this);
	return Math.min(...values);
}

/**
 * Calculates the maximum value of this attribute within the dataset.
 * @returns the maximum value of this attribute within the dataset.
 */
Numeric_Attribute.prototype.max = function() {
	var values = this.dataset_reference.all_attribute_values(this);
	return Math.max(...values);
}

/**
 * @class
 * @classdesc For representing a dataset.
 */
var Dataset = function() {
  /**
   * The name of the dataset. For example, "adult" or "Breast Cancer". (These
   * are the names of two popular datasets.)
   * @member {String}
   */
  this.name = '';
  /**
   * The records which make up the dataset.
   * @member {Records[]}
   */
  this.records = [];
	/**
	 * The Attributes which describe the records within the dataset.
	 * @member {Attribute[]}
	 */
	this.attributes = [];
}
//exports.Dataset = Dataset;

/**
 * Gets the name of the Dataset object.
 * @returns {String} The name of this Dataset object.
 */
Dataset.prototype.get_name = function() {
  return this.name;
}

/**
 * Returns the record from the dataset at the passed index.
 * @param {Number} index The index of the record to get.
 * @returns {Record} The record at the requested index.
 */
Dataset.prototype.get_record = function(index) {
  return this.records[index];
}

/**
 * Returns the number of records in the dataset.
 * @returns {Number} The number of records in the dataset.
 */
Dataset.prototype.get_num_records = function() {
  return this.records.length;
}

/**
 * Returns the attribute from the dataset at the passed index.
 * @param {Number} index The index of the attribute to get.
 * @returns {Attribute} The attribute at the requested index.
 */
Dataset.prototype.get_attribute = function(index) {
  return this.attributes[index];
}

/**
 * Returns the number of attributes in the dataset.
 * @returns {Number} The number of attributes in the dataset.
 */
Dataset.prototype.get_num_attributes = function() {
  return this.attributes.length;
}

/**
 * Sets the name of the dataset.
 * @param {String} name - the new name for this dataset.
 */
Dataset.prototype.set_name = function(name) {
	this.name = name;
}

/**
 * Adds the passed attribute to the dataset.
 * @param {Attribute} attribute - The attribute to add.
 */
Dataset.prototype.add_attribute = function(attribute) {
	this.attributes.push(attribute);
}

/**
 * Adds the passed record to the dataset.
 * @param {Record} record - The record to add.
 */
Dataset.prototype.add_record = function(record) {
	this.records.push(record);
}

/**
 * Returns the index of the requested Attribute object. (-1 if not found)
 * @param {Attribute} attribute - The attribute to find the index of.
 * @returns {Number} The index (-1 if not found).
 */
Dataset.prototype.index_of_attribute = function(attribute) {
	// This function is implemented with guidance from a Mozilla Developer page:
	// http://tinyurl.com/hd874yl
	function find_attribute(attribute_from_dataset) {
		return attribute_from_dataset.name == attribute.name;
	}
	var index = this.attributes.findIndex(find_attribute);
	return index;
}

/**
 * Returns an array of all record values for the requested attribute.
 * @param {Attribute} attribute - The attribute to get the record values of.
 * @return {Value[]} An array of all record values for the requested attribute.
 */
Dataset.prototype.all_attribute_values = function(attribute) {
	var index = this.index_of_attribute(attribute);
	var values = [];
	for (var i = 0; i < this.records.length; i++) {
		values.push(this.records[i].get_value(index).get_value());
	}
	return values;
}
