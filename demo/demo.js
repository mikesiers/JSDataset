// The following solution was taken from http://tinyurl.com/ju6fguj
document.forms['uploadForm'].elements['fileUpload'].onchange = function(evt) {
	if (!window.FileReader) return;	// Browser is not compatible

	var reader = new FileReader();

	reader.onload = function(evt) {
		if (evt.target.readyState != 2) return;
		if (evt.target.error) {
			alert('Error while reading file');
			return
		}

		filecontent = evt.target.result;

		var dataset = parse_ARFF(filecontent);
		console.log(dataset);

		document.forms['uploadForm'].elements['text'].value = evt.target.result;
	};

	reader.readAsText(evt.target.files[0]);
};
